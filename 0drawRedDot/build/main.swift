 
class Bitmap {
    private var colors: [Int: Color] = [:]
    
    public let width: Int
    public let height: Int
    
    public init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    public func set(color: Color, atX x: Int, atY y: Int) {
        guard x >= 0, x < width, y >= 0, y < height else { return }
        let index = y * height + x
        colors[index] = color
    }
    
    public func colorAt(x: Int, y: Int) -> Color? {
        guard x >= 0, x < width, y >= 0, y < height else { return nil }
        let index = y * height + x
        let color = colors[index]
        
        return color
    }
}
struct Color {
    let r: Int
    let g: Int
    let b: Int
    let a: Int
}
struct Matrix4 {

    var a: Float
    var b: Float
    var c: Float
    var d: Float
    
    var e: Float
    var f: Float
    var g: Float
    var h: Float
    
    var i: Float
    var j: Float
    var k: Float
    var l: Float
    
    var m: Float
    var n: Float
    var o: Float
    var p: Float

}
 class PPMConverter {
    static func convert(bitmap: Bitmap) -> String {
        var ppmString = "P3\n\(bitmap.width) \(bitmap.height)\n255\n"
        for y in 0..<bitmap.height {
            for x in 0..<bitmap.width {
                guard let color = bitmap.colorAt(x: x, y: y) else { continue }
                ppmString += "\(color.r) \(color.g) \(color.b) "
            }
            ppmString += "\n"
        }
        
        return ppmString
    }
 }
struct Vector3 {

    var x: Float
    var y: Float
    var z: Float

}
struct Vector4 {

    var x: Float
    var y: Float
    var z: Float
    var w: Float

}
import Foundation

let bitmapWidth = 8
let bitmapHeight = 8

let bitmap = Bitmap(width: bitmapWidth, height: bitmapHeight)
let blackColor = Color(r: 0, g: 0, b: 0, a: 255)
 
for y in 0...bitmapWidth {
    for x in 0...bitmapHeight {
        bitmap.set(color: blackColor, atX: x, atY: y)
    }
}
 
let redDotColor = Color(r: 255, g: 0, b: 0, a: 255)
bitmap.set(color: redDotColor, atX: 4, atY: 4)

let ppmConvertedString = PPMConverter.convert(bitmap: bitmap)
print(ppmConvertedString)

do {
    try ppmConvertedString.write(toFile: "ppmTest.ppm", atomically: false, encoding: .utf8)
}
catch {
    print(error.localizedDescription)
}
