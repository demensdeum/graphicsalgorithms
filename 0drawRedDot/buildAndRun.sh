shopt -s extglob

rm -rf build
mkdir build

cat ../shared/*.swift > ./build/main.swift

cd src
cat main.swift >> ../build/main.swift
cd ..

swift build/main.swift
