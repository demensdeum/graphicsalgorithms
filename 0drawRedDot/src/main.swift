import Foundation

let bitmapWidth = 8
let bitmapHeight = 8

let bitmap = Bitmap(width: bitmapWidth, height: bitmapHeight)
let blackColor = Color(r: 0, g: 0, b: 0, a: 255)
 
for y in 0...bitmapWidth {
    for x in 0...bitmapHeight {
        bitmap.set(color: blackColor, atX: x, atY: y)
    }
}
 
let redDotColor = Color(r: 255, g: 0, b: 0, a: 255)
bitmap.set(color: redDotColor, atX: 4, atY: 4)

let ppmConvertedString = PPMConverter.convert(bitmap: bitmap)
print(ppmConvertedString)

do {
    try ppmConvertedString.write(toFile: "ppmTest.ppm", atomically: false, encoding: .utf8)
}
catch {
    print(error.localizedDescription)
}
