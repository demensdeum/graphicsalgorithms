 
class Bitmap {
    private var colors: [Int: Color] = [:]
    
    public let width: Int
    public let height: Int
    
    public init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    public func set(color: Color, atX x: Int, atY y: Int) {
        guard x >= 0, x < width, y >= 0, y < height else { return }
        let index = y * height + x
        colors[index] = color
    }
    
    public func colorAt(x: Int, y: Int) -> Color? {
        guard x >= 0, x < width, y >= 0, y < height else { return nil }
        let index = y * height + x
        let color = colors[index]
        
        return color
    }
}
struct Color {
    let r: Int
    let g: Int
    let b: Int
    let a: Int
}
import Foundation

struct Matrix4 {

    var name: String?

    var a: Float
    var b: Float
    var c: Float
    var d: Float
    
    var e: Float
    var f: Float
    var g: Float
    var h: Float
    
    var i: Float
    var j: Float
    var k: Float
    var l: Float
    
    var m: Float
    var n: Float
    var o: Float
    var p: Float

    init() {
    
        name = "Unnamed"
    
        a = 0
        b = 0
        c = 0
        d = 0
        
        e = 0
        f = 0
        g = 0
        h = 0
        
        i = 0
        j = 0
        k = 0
        l = 0
        
        m = 0
        n = 0
        o = 0
        p = 0
    }

    func debugPrintout() {
    
        guard let name = name else { return }
        var output = "\(name) matrix\n"
        output += "\(a) \(b) \(c) \(d)\n"
        output += "\(e) \(f) \(g) \(h)\n"
        output += "\(i) \(j) \(k) \(l)\n"
        output += "\(m) \(n) \(o) \(p)"
    
        print(output)
    
    }
    
    static func unitMatrix() -> Matrix4 {
        
        var matrix = Matrix4()
        
        matrix.a = 1
        matrix.b = 0
        matrix.c = 0
        matrix.d = 0
        
        matrix.e = 0
        matrix.f = 1
        matrix.g = 0
        matrix.h = 0
        
        matrix.i = 0
        matrix.j = 0
        matrix.k = 1
        matrix.l = 0
        
        matrix.m = 0
        matrix.n = 0
        matrix.o = 0
        matrix.p = 1
        
        return matrix
    }

    static func perspectiveMatrix(fov: Float, aspect: Float, near: Float, far: Float) -> Matrix4 {
        
        let tanHalfFov = tan(0.5 * fov)
        
        var matrix = Matrix4()
        
		matrix.a = 1 / (aspect * tanHalfFov);
		matrix.b = 0
		matrix.c = 0
		matrix.d = 0
		
		matrix.e = 0
		matrix.f = 1 / tanHalfFov
		matrix.g = 0
		matrix.h = 0
		
		matrix.i = 0
		matrix.j = 0
		matrix.k = far / (near - far)
		matrix.l = -(far * near) / (far - near)
		
		matrix.m = 0
		matrix.n = 0
		matrix.o = 1
		matrix.p = 0
		
		return matrix
        
    }
    
    mutating func translateBy(x: Float, y: Float, z: Float, w: Float = 1.0) {
        
        d = x
        h = y
        l = z
        p = w
        
    }
    
    mutating func scaleBy(x: Float, y: Float, z: Float, w: Float = 1.0) {
        
        a *= x
        f *= y
        k *= z
        p *= w
        
    }
    
    mutating func rotateBy(x: Float, y: Float, z: Float) {
    
        // x rotation
        
        f *= cos(x)
        g *= sin(x)
        j *= -sin(x)
        k *= cos(x)
        
        
        // y rotation
        
        a *= cos(y)
        c *= -sin(y)
        i *= sin(y)
        k *= cos(y)
        
        
        // z rotation
    
        a *= cos(z)
        b *= -sin(z)
        e *= sin(z)
        f *= cos(z)
    
    }
    
}
 class PPMConverter {
    static func convert(bitmap: Bitmap) -> String {
        var ppmString = "P3\n\(bitmap.width) \(bitmap.height)\n255\n"
        for y in 0..<bitmap.height {
            for x in 0..<bitmap.width {
                guard let color = bitmap.colorAt(x: x, y: y) else { continue }
                ppmString += "\(color.r) \(color.g) \(color.b) "
            }
            ppmString += "\n"
        }
        
        return ppmString
    }
 }
struct Vector3 {

    var name: String?
    var x: Float
    var y: Float
    var z: Float

    init(x: Float, y: Float, z: Float) {
        self.name = "Unnamed"
        self.x = x
        self.y = y
        self.z = z
    }
    
    func multiplyBy(matrix: Matrix4) -> Vector3 {
        var vector = self.asVector4()
        
        print("Multiply vector3 by matrix4")
        
        matrix.debugPrintout()
        print("---")
        vector.debugPrintout()
        
        vector.x = matrix.a * vector.x + matrix.b * vector.y + matrix.c * vector.z + matrix.d * vector.w
        vector.y = matrix.e * vector.x + matrix.f * vector.y + matrix.g * vector.z + matrix.h * vector.w
        vector.z = matrix.i * vector.x + matrix.j * vector.y + matrix.k * vector.z + matrix.l * vector.w
        vector.w = matrix.m * vector.x + matrix.n * vector.y + matrix.o * vector.z + matrix.p * vector.w
        
        return vector.asVector3()
    }
    
    func asVector4() -> Vector4 {
        return Vector4(x: x, y: y, z: z, w: 1)
    }
    
    func debugPrintout() {
        guard let name = name else { return }
        let outputString = "\(name)\n\(x) \(y) \(z)"
        print(outputString)
    }
    
}
struct Vector4 {

    var name: String?
    var x: Float
    var y: Float
    var z: Float
    var w: Float
    
    init(x: Float, y: Float, z: Float, w: Float = 1) {
        self.name = "Unnamed"
        self.x = x
        self.y = y
        self.z = z
        self.w = w
    }
    
    func asVector3() -> Vector3 {
        return Vector3(x: x, y: y, z: z)
    }
    
    func debugPrintout() {
        guard let name = name else { return }
        print("\(name)\n\(x) \(y) \(z) \(w)")
    }

}
let dotPosition = Vector3(x: 1, y: 1, z: 1)
var unitMatrix = Matrix4.unitMatrix()
unitMatrix.name = "Unit"
unitMatrix.debugPrintout()
dotPosition.debugPrintout()

var transformMatrix = unitMatrix
transformMatrix.name = "Transform"
transformMatrix.translateBy(x: 10, y: 0, z: 0)
transformMatrix.scaleBy(x: 2, y: 2, z: 2)
transformMatrix.rotateBy(x: 1, y: 1, z: 1)
transformMatrix.debugPrintout()

let multipliedDotPosition = dotPosition.multiplyBy(matrix: transformMatrix)
multipliedDotPosition.debugPrintout()
