let dotPosition = Vector3(x: 1, y: 1, z: 1)
var unitMatrix = Matrix4.unitMatrix()
unitMatrix.name = "Unit"
unitMatrix.debugPrintout()
dotPosition.debugPrintout()

var transformMatrix = unitMatrix
transformMatrix.name = "Transform"
transformMatrix.translateBy(x: 10, y: 0, z: 0)
transformMatrix.scaleBy(x: 2, y: 2, z: 2)
transformMatrix.rotateBy(x: 1, y: 1, z: 1)
transformMatrix.debugPrintout()

let multipliedDotPosition = dotPosition.multiplyBy(matrix: transformMatrix)
multipliedDotPosition.debugPrintout()
