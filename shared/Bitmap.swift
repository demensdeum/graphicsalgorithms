class Bitmap {
    private var colors: [Int: Color] = [:]
    
    public let width: Int
    public let height: Int
    
    public init(width: Int, height: Int) {
        self.width = width
        self.height = height
    }
    
    public func set(color: Color, atX x: Int, atY y: Int) {
        guard x >= 0, x < width, y >= 0, y < height else { return }
        let index = y * height + x
        colors[index] = color
    }
    
    public func colorAt(x: Int, y: Int) -> Color? {
        guard x >= 0, x < width, y >= 0, y < height else { return nil }
        let index = y * height + x
        let color = colors[index]
        
        return color
    }
}
