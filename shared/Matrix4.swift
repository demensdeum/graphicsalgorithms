import Foundation

struct Matrix4 {

    var name: String?

    var a: Float
    var b: Float
    var c: Float
    var d: Float
    
    var e: Float
    var f: Float
    var g: Float
    var h: Float
    
    var i: Float
    var j: Float
    var k: Float
    var l: Float
    
    var m: Float
    var n: Float
    var o: Float
    var p: Float

    init() {
    
        name = "Unnamed"
    
        a = 0
        b = 0
        c = 0
        d = 0
        
        e = 0
        f = 0
        g = 0
        h = 0
        
        i = 0
        j = 0
        k = 0
        l = 0
        
        m = 0
        n = 0
        o = 0
        p = 0
    }

    func debugPrintout() {
    
        guard let name = name else { return }
        var output = "\(name) matrix\n"
        output += "\(a) \(b) \(c) \(d)\n"
        output += "\(e) \(f) \(g) \(h)\n"
        output += "\(i) \(j) \(k) \(l)\n"
        output += "\(m) \(n) \(o) \(p)"
    
        print(output)
    
    }
    
    static func unitMatrix() -> Matrix4 {
        
        var matrix = Matrix4()
        
        matrix.a = 1
        matrix.b = 0
        matrix.c = 0
        matrix.d = 0
        
        matrix.e = 0
        matrix.f = 1
        matrix.g = 0
        matrix.h = 0
        
        matrix.i = 0
        matrix.j = 0
        matrix.k = 1
        matrix.l = 0
        
        matrix.m = 0
        matrix.n = 0
        matrix.o = 0
        matrix.p = 1
        
        return matrix
    }

    static func perspectiveMatrix(fov: Float, aspect: Float, near: Float, far: Float) -> Matrix4 {
        
        let tanHalfFov = tan(0.5 * fov)
        
        var matrix = Matrix4()
        
		matrix.a = 1 / (aspect * tanHalfFov);
		matrix.b = 0
		matrix.c = 0
		matrix.d = 0
		
		matrix.e = 0
		matrix.f = 1 / tanHalfFov
		matrix.g = 0
		matrix.h = 0
		
		matrix.i = 0
		matrix.j = 0
		matrix.k = far / (near - far)
		matrix.l = -(far * near) / (far - near)
		
		matrix.m = 0
		matrix.n = 0
		matrix.o = 1
		matrix.p = 0
		
		return matrix
        
    }
    
    mutating func translateBy(x: Float, y: Float, z: Float, w: Float = 1.0) {
        
        d = x
        h = y
        l = z
        p = w
        
    }
    
    mutating func scaleBy(x: Float, y: Float, z: Float, w: Float = 1.0) {
        
        a *= x
        f *= y
        k *= z
        p *= w
        
    }
    
    mutating func rotateBy(x: Float, y: Float, z: Float) {
    
        // x rotation
        
        f *= cos(x)
        g *= sin(x)
        j *= -sin(x)
        k *= cos(x)
        
        
        // y rotation
        
        a *= cos(y)
        c *= -sin(y)
        i *= sin(y)
        k *= cos(y)
        
        
        // z rotation
    
        a *= cos(z)
        b *= -sin(z)
        e *= sin(z)
        f *= cos(z)
    
    }
    
}
