 class PPMConverter {
    static func convert(bitmap: Bitmap) -> String {
        var ppmString = "P3\n\(bitmap.width) \(bitmap.height)\n255\n"
        for y in 0..<bitmap.height {
            for x in 0..<bitmap.width {
                guard let color = bitmap.colorAt(x: x, y: y) else { continue }
                ppmString += "\(color.r) \(color.g) \(color.b) "
            }
            ppmString += "\n"
        }
        
        return ppmString
    }
 }
