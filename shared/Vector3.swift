struct Vector3 {

    var name: String?
    var x: Float
    var y: Float
    var z: Float

    init(x: Float, y: Float, z: Float) {
        self.name = "Unnamed"
        self.x = x
        self.y = y
        self.z = z
    }
    
    func multiplyBy(matrix: Matrix4) -> Vector3 {
        var vector = self.asVector4()
        
        print("Multiply vector3 by matrix4")
        
        matrix.debugPrintout()
        print("---")
        vector.debugPrintout()
        
        vector.x = matrix.a * vector.x + matrix.b * vector.y + matrix.c * vector.z + matrix.d * vector.w
        vector.y = matrix.e * vector.x + matrix.f * vector.y + matrix.g * vector.z + matrix.h * vector.w
        vector.z = matrix.i * vector.x + matrix.j * vector.y + matrix.k * vector.z + matrix.l * vector.w
        vector.w = matrix.m * vector.x + matrix.n * vector.y + matrix.o * vector.z + matrix.p * vector.w
        
        return vector.asVector3()
    }
    
    func asVector4() -> Vector4 {
        return Vector4(x: x, y: y, z: z, w: 1)
    }
    
    func debugPrintout() {
        guard let name = name else { return }
        let outputString = "\(name)\n\(x) \(y) \(z)"
        print(outputString)
    }
    
}
