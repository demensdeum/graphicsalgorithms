struct Vector4 {

    var name: String?
    var x: Float
    var y: Float
    var z: Float
    var w: Float
    
    init(x: Float, y: Float, z: Float, w: Float = 1) {
        self.name = "Unnamed"
        self.x = x
        self.y = y
        self.z = z
        self.w = w
    }
    
    func asVector3() -> Vector3 {
        return Vector3(x: x, y: y, z: z)
    }
    
    func debugPrintout() {
        guard let name = name else { return }
        print("\(name)\n\(x) \(y) \(z) \(w)")
    }

}
